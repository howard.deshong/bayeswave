#!/bin/bash -e
# -*- coding: utf-8 -*-
# Copyright (C) 2018-2019 James Clark <james.clark@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

set -e

# Allow user to specify install location, otherwise default to conda environment

# check if user-defined installation location is set
if [ -z $1 ] # if empty
then
    install_dir=${CONDA_PREFIX}
    dir_flag=1
else
    install_dir=$1
fi

# Remove trailing slash if present
install_dir=$(echo ${install_dir} | sed 's|/$||' )

# while [ $install_dir = "" ]
# do
#     read -p 'Specify install path: ' install_dir
# done
# echo "Installing to ${install_dir}"

echo "Installing to ${install_dir}"

#
# Configure and make main executables
#
#   START=${PWD}
#   cd src
#   autoreconf --verbose --force --install --make
#   ./configure --prefix ${install_dir} CFLAGS="-Wno-format-truncation -g -O3"
#   make install
#   cd ${START}
./build.sh ${install_dir}

#
# Install python codes
#
pushd BayesWaveUtils
python -m pip install --prefix ${install_dir} --upgrade .
popd

#
# Environment script
#
mkdir -p ${install_dir}/etc
sed "s|INSTALL_DIR|${install_dir}|g" etc/bayeswave-user-env.sh > ${install_dir}/etc/bayeswave-user-env.sh

# Tell user where BW was installed
if [ -z $dir_flag ]
then
    echo ""
    echo "*****************************************************************************"
    echo "  DONE: BayesWave built and installed to: "
    echo "      ${install_dir}"
    echo "  To use: "
    echo "      source ${install_dir}/etc/bayeswave-user-env.sh"
    echo "*****************************************************************************"
else
    echo ""
    echo "*****************************************************************************"
    echo "  DONE: BayesWave built and installed into conda environment: "
    echo "      ${CONDA_DEFAULT_ENV}"
    echo "*****************************************************************************"
fi