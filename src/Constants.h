#define TSUN 4.92569043916e-6                                           // mass to seconds conversion
#define PI2 9.869604401089357992304940125905      // Pi^2
#define TPI 6.2831853071795862319959269370884     // 2 Pi
#define RT2PI 2.5066282746310005024               // sqrt(2 Pi)
#define RTPI 1.772453850905516                    // sqrt(Pi)
#define RT2 1.414213562373095                      // sqrt(2)
#define IRT2 0.707106781186547549                      // 1/sqrt(2)
#define TRT2 2.8284271247461901                  // 2*sqrt(2)
#define LN2 0.6931471805599453                 // ln 2
#define CLIGHT 299792458.0                     // Speed of light (m/s)
#define h22fac  0.31539156525252               //  2. * sqrt(5. / (64.*PI)) factor for h22 to h conversion
#define MPC 3.08568025e22       // Megaparsec in meters 

#define HLdt 0.0106                             // Maximum light travel time allowed H-L
#define HVdt 0.0278                             // Maximum light travel time allowed H-V
#define LVdt 0.0257                            // Maximum light travel time allowed L-V

#define Edt  0.03                              // Allow 26 ms either side of geocenter reference time (accounts for Earth radius and some extra)
#define dtmax 0.03                             // Maximum time shift relative to geocenter waveform in sky likelihood
